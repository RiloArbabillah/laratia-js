<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::truncate();
        \App\User::create([
            'nama' => 'Super Admin',
            'username' => 'super',
            'password' => bcrypt('admin2020')
        ]);
    }
}
